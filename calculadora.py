import matplotlib.pyplot as graficador
import numpy as np
from adjustText import adjust_text
import eel

grafico = "web/graph"
cuenta = 0

eel.init('web')


@eel.expose
def suma(op1, op2):

    res = 0

    try:

        res = complex(op1) + complex(op2)

        graficar_input_resultado(complex(op1), complex(op2), res)

    except ValueError:
        return

    return str(res)


@eel.expose
def resta(op1, op2):

    res = 0

    try:

        res = complex(op1) - complex(op2)

        graficar_input_resultado(complex(op1), complex(op2), res)

    except ValueError:
        return

    return str(res)


@eel.expose
def multiplicacion(op1, op2):

    res = 0
    
    try:
        
        res = complex(op1) * complex(op2)

        graficar_input_resultado(complex(op1), complex(op2), res)

    except ValueError:
        return

    return str(res)


@eel.expose
def division(op1, op2):

    res = 0

    try:
        res = complex(op1) / complex(op2)

        graficar_input_resultado(complex(op1), complex(op2), res)

    except (ValueError, ZeroDivisionError):
        return "Ha introducido valores invalidos o no se puede dividir por 0+0j o 0-0j"

    return str(res)


@eel.expose
def determinante_2x2(val1, val2, val3, val4):
    '''mat = [
        [complex(val1), complex(val2)],
        [complex(val3), complex(val4)]
    ]'''

    det = 0

    try:
        det = (complex(val1) * complex(val4)) - (complex(val3) * complex(val2))
        graficar_determinante_2x2(complex(val1), complex(
            val2), complex(val3), complex(val4), det)
        
    except ValueError:
        return

    return str(det)


@eel.expose
def determinante_3x3(val1, val2, val3, val4, val5, val6, val7, val8, val9):
    '''mat = [
        [complex(val1), complex(val2), complex(val3)],
        [complex(val4), complex(val5), complex(val6)],
        [complex(val7), complex(val8), complex(val9)]
    ]'''

    det = 0

    try:
        det = complex(val1) * (complex(val5) * complex(val9) - complex(val8) * complex(val6)) - complex(val2) * (complex(val4) *
                                                                                                             complex(val9) - complex(val7) * complex(val6)) + complex(val3) * (complex(val4) * complex(val8) - complex(val7) * complex(val5))
        graficar_determinante_3x3(complex(val1), complex(val2), complex(val3), complex(
            val4), complex(val5), complex(val6), complex(val7), complex(val8), complex(val9), det)
        
    except ValueError:
        return
    
    return str(det)


def crear_grafico():
    fig, ax = graficador.subplots()
    for spine in ["left", "bottom"]:
        ax.spines[spine].set_position("zero")

    for spine in ["right", "top"]:
        ax.spines[spine].set_color("none")

    return ax

def iniciar_grafico():

    ax = crear_grafico()
    ax.set_xlim(-40, 40)
    ax.set_ylim(-40, 40)
    ax.set_xlabel("Real", fontsize = 8.5)
    ax.set_ylabel('Imaginario', fontsize = 8.5)
    graficador.xlabel("Real", rotation=0, horizontalalignment="right")
    graficador.ylabel("Imaginario", rotation=90, horizontalalignment="left")
    graficador.figsize=(8, 6)
    graficador.xticks(np.arange(-40, 41, 10.0))
    graficador.yticks(np.arange(-40, 41, 10.0))
    graficador.grid()
    graficador.tight_layout()

    return ax


def graficar_input_resultado(op1, op2, result):

    ax = iniciar_grafico()

    inputs = [op1, op2, result]

    texts = []

    for elem in inputs:
        x = elem.real
        y = elem.imag

        ax.scatter(x, y, 25)
        graficador.plot([0,x],[0,y], color="blue") 
        texts.append(graficador.text(x+0.5, y+0.5, str(elem), fontsize=8))

    adjust_text(texts, arrowprops=dict(arrowstyle='->', color='red'))

    graficador.savefig(nuevo_path_grafico())


def graficar_determinante_2x2(op1, op2, op3, op4, result):

    ax = iniciar_grafico()

    inputs = [op1, op2, op3, op4, result]

    texts = []

    for elem in inputs:
        x = elem.real
        y = elem.imag

        ax.scatter(x, y, 25)
        graficador.plot([0,x],[0,y], color="blue") 
        texts.append(graficador.text(x+0.3, y+0.3, str(elem), fontsize=7))

    adjust_text(texts, arrowprops=dict(arrowstyle='->', color='red'))

    graficador.savefig(nuevo_path_grafico())


def graficar_determinante_3x3(op1, op2, op3, op4, op5, op6, op7, op8, op9, result):

    ax = iniciar_grafico()

    inputs = [op1, op2, op3, op4, op5, op6, op7, op8, op9, result]

    texts = []

    for elem in inputs:
        x = elem.real
        y = elem.imag

        ax.scatter(x, y, 25)
        graficador.plot([0,x],[0,y], color="blue") 
        texts.append(graficador.text(x+0.3, y+0.3, str(elem), fontsize=7))

    adjust_text(texts, arrowprops=dict(arrowstyle='->', color='red'))

    graficador.savefig(nuevo_path_grafico())

    

def nuevo_path_grafico():
    global cuenta
    nuevo_grafico = grafico + str(cuenta) + ".png"

    cuenta = cuenta +1
    
    return nuevo_grafico

@eel.expose
def reiniciar_cuenta ():
    global cuenta
    cuenta = 0

eel.start('main.html')
